package com.cruzbytes.envirosindia.utils;

import android.text.format.DateUtils;

import java.net.NetworkInterface;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;


public class Utils {
    public static String getCurrentDateTime() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS",
                Locale.getDefault());
        return dateFormat.format(Calendar.getInstance().getTime());
    }

    public static String getParsedDate(String createdOn) {
        if (createdOn != null) {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd",
                    Locale.getDefault());
            try {
                Date date = dateFormat.parse(createdOn);
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMM dd , yyyy",
                        Locale.getDefault());

                return simpleDateFormat.format(date);

            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return createdOn;
    }

    public static String getRelativeTime(String createdOn) {
        if (createdOn != null) {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS",
                    Locale.getDefault());
            try {
                Date date = dateFormat.parse(createdOn);
                return DateUtils.getRelativeTimeSpanString(date.getTime(),
                        System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS,
                        DateUtils.FORMAT_ABBREV_RELATIVE).toString();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return createdOn;
    }

    public static String getMacAddress() {
        try {
            List<NetworkInterface> all = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface nif : all) {
                if (!nif.getName().equalsIgnoreCase("wlan0")) continue;
                byte[] macBytes = nif.getHardwareAddress();
                if (macBytes == null) {
                    return "";
                }
                StringBuilder res1 = new StringBuilder();
                for (byte b : macBytes) {
                    res1.append(String.format("%02X:", b));
                }
                if (res1.length() > 0) {
                    res1.deleteCharAt(res1.length() - 1);
                }
                return res1.toString();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "02:00:00:00:00:00";
    }

    public static String getParsedday(String createdOn) {
        if (createdOn != null) {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd",
                    Locale.getDefault());
            try {
                Date date = dateFormat.parse(createdOn);
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEEE",
                        Locale.getDefault());

                return simpleDateFormat.format(date);

            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return createdOn;
    }

    public static String getTime(String createdOn) {
        if (createdOn != null) {
            DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss",
                    Locale.getDefault());
            try {
                Date date = dateFormat.parse(createdOn);
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm a",
                        Locale.getDefault());

                return simpleDateFormat.format(date);

            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return createdOn;
    }

    public static String getDuration(String createdOn) {
        if (createdOn != null) {
            DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss",
                    Locale.getDefault());
            try {
                Date date = dateFormat.parse(createdOn);
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm",
                        Locale.getDefault());

                return simpleDateFormat.format(date);

            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return createdOn;
    }

    public static String getbusCalendardate(String createdOn) {
        if (createdOn != null) {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd",
                    Locale.getDefault());
            try {
                Date date = dateFormat.parse(createdOn);
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy",
                        Locale.getDefault());

                return simpleDateFormat.format(date);

            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return createdOn;
    }

    public static String getbusTravelDate(String createdOn) {
        if (createdOn != null) {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd",
                    Locale.getDefault());
            try {
                Date date = dateFormat.parse(createdOn);
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEEE ,dd MMM yyyy",
                        Locale.getDefault());

                return simpleDateFormat.format(date);

            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return createdOn;
    }
}
