package com.cruzbytes.envirosindia.app;


public class Constant {
    /**
     * Bundle Extras
     */


    public static final int SEATER = 1;
    public static final int EMPLOYEE_ASSIGNED = 2;
    public static final int PICKUP_BUY = 3;


    public static final String USR_NAME = "name";
    public static final String USR_EMAIL = "email";
    public static final String USR_GENDER = "gender";
    public static final String USR_DOB = "dob";
    public static final String USR_PROFILEPIC = "pic";


    public static final String ADDR_ID = "mId";
    public static final String ADDR_STREET = "street";
    public static final String ADDR_AREA = "area";
    public static final String ADDR_CITY = "city";
    public static final String ADDR_STATE = "state";
    public static final String ADDR_COUNTRY = "country";
    public static final String ADDR_ZIPCODE = "zipcode";
    public static final String ADDR_MOBILE = "mobile";
    public static final String KEY_TRAVELDATE = "traveldate";
    public static final String KEY_SOURCEPLACE = "source";
    public static final String KEY_DESTINATIONPLACE = "destination";
    public static final String KEY_SOURCENAME = "sourcename";
    public static final String KEY_DESTINATIONNAME = "destinationname";

    public static final String KEY_SCHEDULECODE = "schedulecode";
    public static final String KEY_OPERATORCODE = "operatorcode";
    public static final String KEY_TRAVELSNAME = "travelername";
    public static final String KEY_BUSTYPE = "bustype";

    public static final String KEY_ARRAY_BOARDINGPPOINT = "boardingpoints";
    public static final String KEY_ARRAY_DROPPINGPPOINT = "droppingpoints";
    public static final String KEY_SELECTEDBOARDINGLOCATION = "selectedlocation";
    public static final String KEY_SELECTEDBOARDINGCODE = "selectedcode";
    public static final String KEY_NOOFTICKETS = "nooftickets";
    public static final String KEY_SELECTEDDROPPINGLOCATION = "selecteddroppinglocation";
    public static final String KEY_SELECTEDDROPPINGCODE = "selecteddroppingcode";
    public static final String KEY_SEATNO = "seatno";
    public static final String KEY_SEATFARE = "seatfare";
    public static final String KEY_CANCELLATIONTERMS = "cancellationterms";

    public static final String KEY_BUSFACILITY = "busfacility";
    public static final String KEY_BUSTYPEMODEL = "bustypemodel";
    public static final String KEY_PARTIALCANCELLATIONS = "partialcancellations";
    public static final String KEY_ARRAY_PASSENGERDETAILS = "passengerDetails";
    public static final String KEY_TOTALFARE = "totalbusfare";

}
